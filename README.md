# ChungCraft

A Minecraft Client (and server eventually) written in C++ using OpenGL.
Available for Windows and Linux, with planned Mac OS and Android support.

# Downloads:

The project is still in development, there are currently no builds available. If you want to, you can build the project yourself.

# Planned Features:
ChungCraft Dedicated Server\
Multiplayer\
Controller Support\
Touch Control Support\
VR Support\
Vanilla and OptiFine Resource Pack Support\
Vanilla Datapack Support

# Possible Features that would be cool to have:
Mod Support (Lua or Wren (Wren is a Lua competitor https://wren.io/) and JSON, maybe custom DLL support)\
Fabric and Forge Client Interaction\
AR\
Raytracing\
Fabric and/or Forge Mod Loading Support\
Vulkan Support\
Multiversion Support (Like ViaVersion)\
Bedrock Support (Possibly using Geyser somehow)

# Contributing:

*This section is still being written! (~~Feel free to contribute to it~~)*

You can submit a poll request on GitLab

# Building:

*This section is still being written! (Feel free to contribute to it)*

### Linux:

Install the dependencies (TODO: List dependencies here)\
You can run **BUILD-RUN-LIN.sh**, which as the name suggests, it will build and run ChungCraft.

### Windows:

Install the dependencies (TODO: List dependencies here)\
You can run **BUILD-RUN-WIN.bat**, which as the name suggests, it will build and run ChungCraft.

### Mac OS:

Install the dependencies (TODO: List dependencies here)\
~~You can run BUILD-RUN-MAC.sh, which as the name suggests, it will build and run ChungCraft.~~

## Android:

### Using Linux:
Install the dependencies (TODO: List dependencies here)\
~~You can run BUILD-RUN-ANDROID-USING-LINUX.bat, which as the name suggests, it will build ChungCraft~~

### Using Windows:
Install the dependencies (TODO: List dependencies here)\
~~You can run BUILD-RUN-ANDROID-USING-WIN.bat, which as the name suggests, it will build ChungCraft~~

### Using Mac OS:
Install the dependencies (TODO: List dependencies here)\
~~You can run BUILD-RUN-ANDROID-USING-MAC.bat, which as the name suggests, it will build ChungCraft~~