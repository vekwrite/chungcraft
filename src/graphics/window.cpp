#include "window.h"

#include <GL/glew.h>
#include <iostream>

Window::Window(int width, int height, const std::string& title) {
    std::cout << "Attempting to create new window with width [" << width << "] and height [" << height << "] with title [" << title << "]\n";

    // Init SDL
    SDL_Init(SDL_INIT_EVERYTHING);

    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    _window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);
    _glContext = SDL_GL_CreateContext(_window);

    // Init GLEW
    GLenum status = glewInit();

    if(status!=GLEW_OK) {
        std::cerr << "GLEW failed to initialize!\n";
    }

    _isClosed = false;

    glEnable(GL_DEPTH_TEST);

    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    //0 = No FPS Limit
    //1 = 60 FPS Vsync
    //2 = 30 FPS Vsync
    SDL_GL_SetSwapInterval(1);
}

Window::~Window() {
    SDL_GL_DeleteContext(_glContext);
    SDL_DestroyWindow(_window);
    SDL_Quit();
}

void Window::Clear() {
        //The default glClearColor, black would be too bland. So why not dark purple! :) -Endode
        glClearColor(0.1f, 0.0f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

bool Window::IsClosed(){
    return _isClosed;
}

void Window::SwapWindow() {
    SDL_GL_SwapWindow(_window);

    //SDL_Event e;

    //while(SDL_PollEvent(&e)) {
    //    if(e.type == SDL_QUIT) {
    //        _isClosed = true;
    //    }
    //}
    
}