#include "mesh.h"
#include <vector>
#include "../util/obj_loader.h"

Mesh::Mesh(const std::string& fileName) {
	IndexedModel model = OBJModel(fileName).ToIndexedModel();
	InitMesh(model);
}

Mesh::Mesh(Vertex* vertices, unsigned int numVerticies, unsigned int* indices, unsigned int numIndices) {

    IndexedModel model;

    for(unsigned int i = 0; i < numVerticies; i++) {
        model.positions.push_back(*vertices[i].getPos());
        model.texCoords.push_back(*vertices[i].getTexCoord());
        model.normals.push_back(*vertices[i].getNormal());
    }

    for(unsigned int i = 0; i < numIndices; i++) {
        model.indices.push_back(indices[i]);
    }

    InitMesh(model);

    //_drawCount = numVerticies;
    // _drawCount = numIndices;

    // glGenVertexArrays(1, &_vertexArrayObject);
    // glBindVertexArray(_vertexArrayObject);

    // std::vector<glm::vec3> positions;
    // std::vector<glm::vec2> texCoords;

    // positions.reserve(numVerticies);
    // texCoords.reserve(numVerticies);

    // for(unsigned int i = 0; i < numVerticies; i++) {
    //     positions.push_back(*vertices[i].getPos());
    //     texCoords.push_back(*vertices[i].getTexCoord());
    // }

    // glGenBuffers(NUM_BUFFERS, _vertexArrayBuffers);

    // //positions
    // glBindBuffer(GL_ARRAY_BUFFER, _vertexArrayBuffers[POSITION_VB]);
    // glBufferData(GL_ARRAY_BUFFER, numVerticies * sizeof(positions[0]), &positions[0], GL_STATIC_DRAW);

    // glEnableVertexAttribArray(0);
    // glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

    // //texcoord
    // glBindBuffer(GL_ARRAY_BUFFER, _vertexArrayBuffers[TEXCOORD_VB]);
    // glBufferData(GL_ARRAY_BUFFER, numVerticies * sizeof(texCoords[0]), &texCoords[0], GL_STATIC_DRAW);

    // glEnableVertexAttribArray(1);
    // glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

    // //index_vb
    // glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vertexArrayBuffers[INDEX_VB]);
    // glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices * sizeof(indices[0]), &indices[0], GL_STATIC_DRAW);


    // glBindVertexArray(0);
    // glBindVertexArray(1);
}

Mesh::~Mesh() {
    glDeleteVertexArrays(1, &_vertexArrayObject);
}

void Mesh::InitMesh(const IndexedModel& model) {
    _drawCount = model.indices.size();

    glGenVertexArrays(1, &_vertexArrayObject);
    glBindVertexArray(_vertexArrayObject);


    glGenBuffers(NUM_BUFFERS, _vertexArrayBuffers);

    //positions
    glBindBuffer(GL_ARRAY_BUFFER, _vertexArrayBuffers[POSITION_VB]);
    glBufferData(GL_ARRAY_BUFFER, model.positions.size() * sizeof(model.positions[0]), &model.positions[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

    //texcoord
    glBindBuffer(GL_ARRAY_BUFFER, _vertexArrayBuffers[TEXCOORD_VB]);
    glBufferData(GL_ARRAY_BUFFER, model.positions.size() * sizeof(model.texCoords[0]), &model.texCoords[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

    //normals
    glBindBuffer(GL_ARRAY_BUFFER, _vertexArrayBuffers[NORMAL_VB]);
    glBufferData(GL_ARRAY_BUFFER, model.normals.size() * sizeof(model.normals[0]), &model.normals[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);

    //index_vb
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vertexArrayBuffers[INDEX_VB]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, model.indices.size() * sizeof(model.indices[0]), &model.indices[0], GL_STATIC_DRAW);


    glBindVertexArray(0);
    glBindVertexArray(1);
}

void Mesh::Draw() {
    glBindVertexArray(_vertexArrayObject);

    glDrawElements(GL_TRIANGLES, _drawCount, GL_UNSIGNED_INT, 0);
    //glDrawArrays(GL_TRIANGLES, 0, _drawCount);

    glBindVertexArray(0);
}