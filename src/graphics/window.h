#pragma once

#include <SDL.h>
#include <string>

class Window {
public:
    Window(int width, int height, const std::string& title);
    virtual ~Window();

    void SwapWindow();
    void Clear();
    bool IsClosed();
    inline void SetShouldClosed(bool shouldClose) { _isClosed = shouldClose; }
    inline std::string GetWindowTitle() { return SDL_GetWindowTitle(_window); }

    //TODO: Toggle Texture Filtering (setTextureFiltering(bool))

private:
    Window(const Window& other) {}
    void operator=(const Window& other) {}

    SDL_Window* _window;
    SDL_GLContext _glContext;
    bool _isClosed;
};