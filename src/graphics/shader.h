#pragma once

#include <string>
#include <GL/glew.h>
#include "../transform.h"
#include "camera.h"

class Shader {
    public:
        Shader(const std::string& fileName);

        void Bind();
        void Update(const Transform& transform, const Camera& camera);

        virtual ~Shader();
    private:
        static const unsigned int NUM_SHADERS = 2;
        Shader(const Shader& other) {}
        void operator=(const Shader& other) {}

        enum {
            TRANSFORM_U,

            NUM_UNIFORMS
        };

        GLuint _program;
        GLuint _shaders[NUM_SHADERS];
        GLuint _uniforms[NUM_UNIFORMS];
};