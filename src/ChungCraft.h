#pragma once

namespace Version {
    int VERSION_MAJOR=	   0;
	int VERSION_MINOR=	   8;
	int VERSION_REVISION=  0;
	std::string ADDITIONAL_VERSION_INFO=		"Alpha";
	int BUILD_NUMBER=	   0;
	std::string VERSION = std::to_string(VERSION_MAJOR) + "." + std::to_string(VERSION_MINOR) + "." + std::to_string(VERSION_REVISION) + " " + ADDITIONAL_VERSION_INFO;
	
	std::string VERSION_TITLE=					"Whatever 'Update'";
};