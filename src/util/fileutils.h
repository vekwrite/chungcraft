#pragma once

#include <iostream>
#include <fstream>

std::string LoadFileAsString(const std::string& fileName) {
    std::ifstream file;
    file.open((fileName).c_str());

    std::string output;
    std::string line;

    if(file.is_open())
    {
        while(file.good())
        {
            getline(file, line);
			output.append(line + "\n");
        }
    }
    else
    {
		std::cerr << "Unable to load file: " << fileName << std::endl;
    }

    //std::cout << "Contents: [" << "]" << output << std::endl;

    return output;
}