#pragma once

#include <glm.hpp>

class Block {
    public:
        Block();
        ~Block();

    private:
        glm::vec3 _posInChunk;
}