#pragma once

#include "entity.h"

class LivingEntity : public Entity {
    public:
        LivingEntity(Entity entity);
        ~LivingEntity();

        inline int GetHealth() {return _health;}
        inline void SetHealth(int newHealth) {_health = newHealth;}
        void Damage(int amount);
    private:
        int _health;
        bool _isDead = false;
        //TODO: Armor, Current Potion Effects, Inventory (inventory depends tho), Current Item (related to inventory, wont really be different (in inventory class then will be like, hotbar.0 and that will be the first hotbar slot and the hand of like a zombie or something))
}