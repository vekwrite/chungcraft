#pragma once

#include <string>
#include <glm.hpp>

class Entity {
    public:
        Entity(std::string name = "Entity", glm::vec3 pos = glm::vec3(0,0,0));
        ~Entity();

        inline std::string GetName() { return _name; }
        inline glm::vec3 GetPosition() { return _pos; }

        inline void SetName(std::string newName) { _name = newName; }
        inline void SetPosition(glm::vec3 newPos) { _pos = newPos; }

        void Damage(int amount);

    private:
        std::string _name;
        glm::vec3 _pos;
}