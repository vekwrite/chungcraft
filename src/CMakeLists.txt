cmake_minimum_required(VERSION 3.12)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/build/unknown/)

if(WINDOWS_USING_LINUX)
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/build/windows/)
    set(CMAKE_C_COMPILER x86_64-w64-mingw32-gcc)
    set(CMAKE_CXX_COMPILER x86_64-w64-mingw32-g++)
    add_definitions(-DGLEW_STATIC)
else()
    if(WIN32)
        set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/build/windows/)
        set(CMAKE_C_COMPILER gcc)
        set(CMAKE_CXX_COMPILER g++)
        add_definitions(-DGLEW_STATIC)
    elseif(UNIX)
        set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/build/linux/)
        set(CMAKE_C_COMPILER gcc)
        set(CMAKE_CXX_COMPILER g++)
    elseif(APPLE)
        set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/build/apple/)
    endif()
endif()

project(ChungCraft VERSION 0.1.0)

include_directories(ChungCraft include_dir ${CMAKE_BINARY_DIR}/dependencies/include)
link_directories(ChungCraft lib_dir ${CMAKE_BINARY_DIR}/dependencies/lib)

#Don't need to include header files here since they are already included in the cpp files
#file(GLOB src CONFIGURE_DEPENDS *.cpp *.h */*.h */*.cpp)
#file(GLOB include_dir CONFIGURE_DEPENDS *.cpp *.h */*.h */*.cpp)

file(GLOB src CONFIGURE_DEPENDS *.cpp */*.cpp)
file(GLOB include_dir CONFIGURE_DEPENDS *.cpp */*.cpp)

add_executable(ChungCraft ${src} ${include_dir})
if(WINDOWS_USING_LINUX)
    target_link_libraries(ChungCraft glew32s opengl32 SDL2main SDL2test SDL2)
else()
    if(WIN32)
        target_link_libraries(ChungCraft glew32s opengl32 SDL2main SDL2test SDL2)
    elseif(UNIX)
        target_link_libraries(ChungCraft GL GLEW SDL2)
    endif()
endif()