#include <iostream>
#include <string>
#include <GL/glew.h>

#include "ChungCraft.h"
#include "graphics/window.h"
#include "graphics/shader.h"
#include "graphics/mesh.h"
#include "graphics/texture.h"
#include "transform.h"
#include "graphics/camera.h"
#include "input/input.h"

#define WIDTH 1024
#define HEIGHT 576

int main(int argc, char *args[]) {
    std::cout << "Hello World, this is ChungCraft V" << Version::VERSION << std::endl;
    std::cout << "Build Number (Auto incrementing build number not implemented yet): " << Version::BUILD_NUMBER << std::endl;
    std::cout << "Version Title: " << Version::VERSION_TITLE << std::endl;

    Window window(WIDTH, HEIGHT, "ChungCraft V" + Version::VERSION + " | " + Version::VERSION_TITLE);
    Shader shader("../../res/shaders/basic");
    Texture texture("../../res/textures/dev/vekgine_dev_texture.png");
    // Texture texture("../../res/textures/vekdefaultpack/blocks/grass_block.png");
    //TODO: Print OpenGL version (do it here, or in window.cpp/window.h)
    Transform transform;

    Camera camera(glm::vec3(0,0,-4), 70.0f, (float)WIDTH/(float)HEIGHT, 0.01f, 1000.0f);

    Input input;

    float counter = 0.0f;

    // Vertex vertices[] = {Vertex(glm::vec3(-0.5, -0.5, 0), glm::vec2(0,0)), 
    //                      Vertex(glm::vec3(0, 0.5, 0), glm::vec2(0.5,1.0)), 
    //                      Vertex(glm::vec3(0.5, -0.5, 0), glm::vec2(1.0,0)), 
    // };

    //unsigned int indices[] = {0, 1, 2};

    //Mesh mesh(vertices, sizeof(vertices)/sizeof(vertices[0]), indices, sizeof(indices)/sizeof(indices[0]));
    //Mesh mesh2("../../res/models/monkey3.obj");

    // Vertex voxelVerticies[] = {

    // };

    //unsigned int voxelIndices[] = {};

    //Mesh voxelMesh(voxelVertices, sizeof(voxelVertices)/sizeof(voxelVertices[0]), voxelIndices, sizeof(voxelIndices)/sizeof(voxelIndices[0]));

    Mesh voxelMeshOBJ("../../res/models/block.obj");

    Uint32 lastTime = SDL_GetTicks();
    Uint32 currentFPS;
    Uint32 framesFPS = 0;

    while (!window.IsClosed()) {

        window.SwapWindow();

        input.Poll(window);

        if (input.IsKeyDown(SDLK_ESCAPE)) {
            std::cout << "Closing " << window.GetWindowTitle() << std::endl;
            window.SetShouldClosed(true);
        }

        //TODO: Remove This
        transform.GetRot().y = counter;

        window.Clear();
        shader.Bind();
        texture.Bind(0);
        shader.Update(transform, camera);
        // mesh.Draw();
        // mesh2.Draw();
        voxelMeshOBJ.Draw();

        framesFPS++;
        if (lastTime < SDL_GetTicks() - 1.0*1000) {
            lastTime = SDL_GetTicks();
            currentFPS = framesFPS;
            framesFPS = 0;
        }
        std::cout << "FPS: " << currentFPS << std::endl;

        counter += 0.01f;
    }

    return 0;
}