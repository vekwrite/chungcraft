#pragma once

#include <iostream>
#include <SDL.h>
#include "../graphics/window.h"

#define MAX_KB_KEYS 1024
#define MAX_MOUSE_BUTTONS 32

class Input {
public:
	Input();
	~Input();

	void Poll(Window& window);

	bool IsKeyDown(SDL_Keycode keycode);
	bool IsKeyPressed(SDL_Keycode keycode);
	bool IsKeyReleased(SDL_Keycode keycode);
	
	//TODO: Make versions of Down, Pressed, and Released but when any key is pressed/released/down (IsAnyKeyDown/IsAnyKeyPressed/IsAnyKeyReleased)

private:
	bool _keyboardMap[MAX_KB_KEYS] = {false};
	bool _keyboardMapPressed[MAX_KB_KEYS] = {false};
	bool _keyboardMapReleased[MAX_KB_KEYS] = {true};

};