#include "input.h"
Input::Input() {

}

Input::~Input() {

}

void Input::Poll(Window& window) {

	//SDL_PumpEvents();

	SDL_Event e;

	while (SDL_PollEvent(&e)) {
		//ImGui_ImplSDL2_ProcessEvent(&e);
		if (!e.key.repeat) {
			switch (e.type) {
			case SDL_QUIT:
				window.SetShouldClosed(true);
			case SDL_KEYDOWN:
				//std::cout << "KEYDOWN " << e.key.keysym.sym << std::endl;
				_keyboardMap[e.key.keysym.sym] = true;
				_keyboardMapPressed[e.key.keysym.sym] = true;
				_keyboardMapReleased[e.key.keysym.sym] = false;
				break;
			case SDL_KEYUP:
				//std::cout << "KEYUP " << e.key.keysym.sym << std::endl;
				_keyboardMap[e.key.keysym.sym] = false;
				_keyboardMapPressed[e.key.keysym.sym] = false;
				_keyboardMapReleased[e.key.keysym.sym] = true;
				break;
			}
		}


	}

}

//True whilst key is held down
bool Input::IsKeyDown(SDL_Keycode keycode) {

	if (keycode > MAX_KB_KEYS) {
		return false;
	} else {
		return _keyboardMap[keycode];
	}

}

//True first frame key is down
bool Input::IsKeyPressed(SDL_Keycode keycode) {
	if (keycode > MAX_KB_KEYS) {
		return false;
	} else {
		if (_keyboardMapPressed[keycode]) {
			_keyboardMapPressed[keycode]=false;
			return true;
		} else {
			return false;
		}
	}
}

//True first frame key is released
bool Input::IsKeyReleased(SDL_Keycode keycode) {
	if (keycode > MAX_KB_KEYS) {
		return false;
	} else {
		if (_keyboardMapReleased[keycode]) {
			_keyboardMapReleased[keycode] = false;
			return true;
		} else {
			return false;
		}
	}
}